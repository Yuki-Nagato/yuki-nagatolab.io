---
layout: friends
title: Links
---
Some links may have become inactive or transfered. Please be aware and verify their validity.

- <a href="https://ekyu.moe/" target="_blank">
    <img src="https://github.com/Equim-chan.png">
    <span>Equim</span>
    <span>失う</span>
    </a>
- <a href="https://csuwangj.github.io/" target="_blank">
    <img src="https://github.com/CSUwangj.png">
    <span>CSUwangj</span>
    <span>LuckyDog</span>
    </a>
- <a href="http://spica.cloud/" target="_blank">
    <img src="https://github.com/Hatsunespica.png">
    <span>Spica</span>
    <span>Hatsunespica</span>
    </a>
- <a href="https://inspurer.github.io/" target="_blank">
    <img src="https://inspurer.github.io/images/avatar.jpg">
    <span>月小水长</span>
    <span>饮冰室主人</span>
    </a>
- <a href="https://blog.ny-do.com/home/" target="_blank">
    <img src="https://blog.ny-do.com/assets/avatar/avatar.small.png">
    <span>Donny</span>
    <span>Hello, welcome to... Wait, stop clicking my avatar!</span>
    </a>
- <a href="https://adameta.top/" target="_blank">
    <img src="https://github.com/oceanlvr.png">
    <span>oceanlvr</span>
    <span>坚持写点东西</span>
    </a>
- <a href="https://edwardzcn.github.io/" target="_blank">
    <img src="https://eddyblog.oss-cn-shenzhen.aliyuncs.com/Blog/favicon.jpg">
    <span>Edwardzcn</span>
    <span>为己活 天地活 坦荡活 信仰活</span>
    </a>
- <a href="https://www.addesp.com/" target="_blank">
    <img src="https://www.addesp.com/avatar">
    <span>ADD-SP</span>
    <span>记录 & 分享 & 感受</span>
    </a>
- <a href="https://anillc.cn/" target="_blank">
    <img src="https://gravatar.loli.net/avatar/5df946d48b36e6f8061cdfe7ebcdf75c">
    <span>Anillc</span>
    <span>Anillc's Blog</span>
    </a>
- <a href="https://tianxianzi.me/" target="_blank">
    <img src="https://tianxianzi.me/images/favicon.png">
    <span>天仙子</span>
    <span>人亦有言，日月于征。安得促席，说彼平生。</span>
    </a>
