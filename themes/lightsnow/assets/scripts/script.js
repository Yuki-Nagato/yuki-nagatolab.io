"use strict";

const Like = {
    apiUrl: 'https://api.yuki-nagato.com/api/counter?key=blog.yuki-nagato.com',
    ready: false
};

function sleep(time) {
    return new Promise(resolve => setTimeout(resolve, time));
}

async function getLike() {
    const resp = await (await fetch(Like.apiUrl + "&method=get")).text();
    for (const it of document.getElementsByClassName("like-count")) {
        it.innerText = resp;
    }
    Like.ready = true;
}

async function addLike() {
    if (!Like.ready) return;
    Like.ready = false;
    document.getElementById("like-text").innerText = "I like you too (*≧▽≦)";
    document.getElementById("t-nav-like").style.color = "#e51c23";
    const resp = await (await fetch(Like.apiUrl + "&method=add")).text();
    for (const it of document.getElementsByClassName("like-count")) {
        it.innerText = resp;
    }
    await sleep(2000);
    document.getElementById("like-text").innerText = "Do you like me?";
    document.getElementById("t-nav-like").style.color = "";
    Like.ready = true;
}

getLike();
document.getElementById("t-nav-like").onclick = addLike;
document.getElementById("l-card-like").onclick = addLike;

const players = [];
for (const container of document.getElementsByClassName("myplayer")) {
    players.push(new APlayer({
        container: container,
        audio: JSON.parse(container.dataset.audio),
        preload: 'none'
    }));
}