---
title: Door
time: 2024-02-10T21:30:00+08:00
categories: [essay]
toc: !!bool true
---

## Preface (in 2024)

Happy New Year!

Today is the first day of the Lunar New Year. The past year has been a new beginning for us. On some levels, people’s attention to science fiction has never been as enthusiastic as in 2023. I remembered that I had written a science fiction (fantasy) novel before, titled “The Door”. This novel was written in 2015. At that time, besides me, there were several other authors, but I can’t remember who they were. I’m really sorry. I want to post this novel here today as part of my annual summary and the first post of the new year. Since most of the content is written by me, and no one cares about the content, there should be no copyright issues.

The following is the text.

## Cover

![Cover](assets/cover.opti.png)

<!-- more -->

## Prologue

This is a point in the universe, 345 km above the sea level of earth. How far is it? You can say it is not so far for the thickness of atmosphere is above 1000 km. you can also say it is far enough, for 200 km higher than the mesosphere, in the thermosphere of the ionosphere. The air is ionized into positive and negative ion, they attract each other, and then get impact, to absorb and release the heat endlessly.

Time, a wonderful thing among the universe, we don’t know which is elder. However, ‘Universe have “begin”, but with no “end”. Stars have “begin”, but will subduct because of the loss of energy.’ Although the sentence is mysterious, it shows the power of ‘time’. Maybe, only universe itself can be the opponent of ‘time’. If someone have technology to control time, that person will be the ruler of the world.

Sometime, 345 km above the sea level of earth, a space station appeared. Supposing that someone in the space station look out of the window, he will find that he is geosynchronous, and just below him, there is a playground with 200m track. But nobody will do that anymore, just like a little camera abandoned by people in open country will never be used to take photos. The space station is forgotten by human forever, and nobody will find it again.

> The next person who will never turn up:
>
> I’m the last human being in the ISS, and also the last lonely one. The place is the end of all, or maybe, it is also the beginning of all. However, I don’t care of this anymore.
>
> I was already lost every hope of the research these years, the result is even worse. I can’t break the established cause and effect. Besides, as the last person, I can’t finish my mission. Perhaps, this is the meaning of our human race existence,

This is the ending part of the last experiment report in space station, it is neither experimental results nor experimental conclusion, it is a letter with no end, the second part seems to be destroyed by somebody on purpose, only the front half part remains.

## Chapter 0

Hot summer.

‘Jesus! Why the power is cut in such this weather? Why not prepare an emergency power supply?’

‘Even if there is, it won’t be used in air-conditioner. Although our school’s air-conditioner is energy-efficient, every class uses it at the same time, there will be a large power consumption.’

The student beside me was talking about the ‘power-cut’ like this. But as a reasonable human-being, I also hated it bitterly. Barely had 5 lessons this morning, I had no appetite to have lunch.

But I had to. I stood up, and soon got vertigo because of a long-time lay on the desk. I touched the wall, took few deep breathing until the bad feelings gone. Then I took my spoons, and went to the cafeteria with some of my friends like before.

I would never know, that suffocating lunch, will be the beginning of the incident I will never forget whole my life.

## Chapter 1

The lunch was as before, it tastes sweet when you eat the rice, and tastes bitter when you have the dishes. You can only taste it when the mixed up.

After the meal, we went for washing our hands as usual. Besides that, we also played a game at this time, it may sounds boring. The game is the people who finish washing early will hide behind the door wait for the people who finish the last. And when he come out, we jump out and scream to make him surprised. Though it is boring through my words, it was interesting for us, as the hidden place & reply strategies are multiple. We had played it many times.

I, the last person who finished washing, left the restroom. I held my breath and headed towards the school building I had to pay attention on every corner. Because of a large amount of experience I had, I could analyse where those people should be clearly.

‘This time is the corner of that door,’ I thought.

The door was located in -1/2 F, North building. It always locked and there were a sign on the door writing bilingually: 闲人莫进 (STAFF ONLY). I was closing to the door silently, and kick him rapidly. Of course, he was startled. But when I was going to run, I found something strange.

Despite of the material of the door was metal, there were windows on it. We couldn’t see anything through that window before. However, this time, there were light coming out of the room, and we could see clearly the light was from an old incandescent light bulb. My friends also noticed the strange about the room. We all peeped inside.

The room was in a mess. There were rough wooden articles everywhere, and wood chips on the floor. Was that a carpenter classroom before? No, it shouldn’t be, since those articles were gigantic. It was like a room of processing wooden articles rather than a classroom. Those unprocessed articles were piled up.

But these were all my guess. We couldn’t confirm what exactly the room was. And the most important thing was, why the lights had been turned on. It couldn’t be the janitor suddenly had a brainstorm and wanted to clean the room. We couldn’t see any living things through the window.

## Chapter 2

A room with light proves someone has been in it. So this room must have somebody in. We looked at each other, and put our hand on the doorknob together.

Locked… as we expected. However, it could still be opened by other ways. We kicked the door hard, and the door just flew away. We held our breath for a while but nothing happened. Then, we stepped in.

Suddenly, we heard a voice, from our trouser pockets. We put our hands into the pocket, and took out our mobile phone.

All of us were astonished by the things in our hands.

It was our mobile phone of course, except the strips we couldn’t understand on the screen, just like a TV without signal, and also the white noise. You can imagine the feeling of a broken TV and radio. But the most important thing is, our cell phone had been shut down before we went to school.

I presumed that there must had a kind of electromagnetic wave which made our cell phone received the signals and came out the noise and pictures. We didn’t know whether it is harmful to human or not.

Therefore, I suggested us leaving there as soon as possible before we resolved this problem.

## Chapter 3

The following day, we were still having our lunch in the stuffy basement.

‘Should we switch on the air conditioner?’ The girl beside me complained.

I deeply knew that this was impossible, for there was a ventilation pipe lead to the ground. Somehow our teachers always hope it completes the mission of refrigeration, they just ignored the temperature of ground was above 35℃ as well.

‘Shall we go to that weird room again?’

I was waiting for this, for I prepared ‘Geiger counter’, by which I can detect the harmfulness of electromagnetic wave clearly.

‘Let’s go. And this time we step a little more inside, to find out what on earth is in there.’

I agreed without words.

## Chapter 4

We stood in front of the door, hold our breath, ready to open it.

The door opened… not by us.

It was power of the earth. The building was shaken wildly.

The earthquake! Stairs was blocked by the articles fallen from upstairs. We could just hide in a corner of that room, the woodworks in the room created a safe, triangle-shaped zone.

I took out my cell phone, try to contact to outside. But again, those mysterious stripes fill up the screen. What the hell? I put out my Geiger counter, and just before I could see the number on it, the woodworks crashed, a huge concrete flew to us.

## Chapter 5

This is a point in the universe, 345 km above the sea level of the Earth. How far is it? You can say it is not so far, for the thickness of atmosphere is above 1000 km. You can say it is far enough as well, for the place is 200 km higher than the mesosphere, in the thermosphere of the ionosphere. The air is ionized positive and negative ion. They attract each other, and get impact, in order to absorb and release the heat endlessly.

I’m wondering the reason I stand in that space station, so I look forward to the monitor of space station, it shows the UTC time and the constant graphics of command center.

To my surprise, the time is 31 August, 2006. Although I don’t know what exactly the date today is, the time is long before I could remember. However, the more horrible thing is, there’s nobody in command center.

I search for the interphone in my space suit at once, but I fail. I suddenly remember that I oughtn’t to know what is interphone, inside my clothes pocket should be my cell phone!

I remember all, including what’s happening after the concrete flew to me. At that moment, my cell-phone was broken, and I slipped into a coma. After I waked up, I was here.

Looks like the time has been reset. The cell-phone brought me back to 2006, and only me. At this time, the earthquake has not happened, but the human race doesn’t exist anymore. Only me in this world.

I don’t know 9 years later whether the earthquake will happen or not, however, it doesn’t really matter, since human is extinct. ‘Maybe there is somebody like me back to this time by the same way, to the place I’m in.

So, I write a letter to that person who will never come.

The ending is:

> Despite of that, I believe I’m not alone. The restrain of time can be broken. If you find this letter, congratulations, you are the master of time. I believe you can go back, and tell your friends all you know.