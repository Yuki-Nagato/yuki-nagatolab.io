---
title: 使用systemd部署.NET源码
time: 2021-05-11T00:30:00+08:00
categories: [技术]
tags: [systemd,.NET]
toc: !!bool false
---
好久没有写东西了。

这篇文章主要是记录一下如何手动配置systemd服务，尤其是用.NET开发的长期运行的网络服务，并且无需手动编译部署包。

开发.NET服务最优雅的方式莫过于使用Visual Studio写C#，在Windows上无论是编写还是调试都很方便。不过写好之后如何将它部署到服务器上呢？

[.NET提供了一些生成部署包的方法，包括生成跨平台二进制文件、依赖框架的可执行文件和独立可执行文件的方法。](https://docs.microsoft.com/zh-cn/dotnet/core/deploying/)不过经过我一段时间的测试，发现这些方法还是有些麻烦的，每次更新后都需要手动发布然后上传部署包。而最优雅的方法应当是直接同步代码，然后服务器用SDK直接运行代码。

这里贴上我在服务器上部署的一个.NET服务的配置文件和命令，作为参考。

新建`/etc/systemd/system/webmonitor.service`配置：

```ini
[Unit]
Description=Yuki Web Monitor
After=network.target  # 该服务依赖网络服务，在启动网络服务后再启动该服务

[Service]
Type=simple
User=lighthouse  # 执行程序的user和group
Group=lighthouse
WorkingDirectory=/home/lighthouse/WebMonitor/WebMonitor  # 设置CWD，注意应指向项目（Project）目录而不是解决方案（Solution）目录
ExecStart=/usr/bin/dotnet run  # 如果需要其他参数写在后面即可
Restart=on-failure  # 异常自动重启

[Install]
WantedBy=multi-user.target  # 允许开机自动启动
```

添加该配置文件后，执行：

```
sudo systemctl enable webmonitor.service  # 开机启动
sudo systemctl disable webmonitor.service  # 禁用开机启动
sudo systemctl restart webmonitor.service  # 启动、重启动该服务
sudo systemctl stop webmonitor.service  # 停止该服务
```
