---
title: 记一次SSH公钥配置无效的问题——与SELinux的初次交锋
time: 2020-05-05T14:42:04+08:00
categories: [技术]
tags: [SSH,SELinux]
---
## 先说最终解决方法

执行命令

```
chcon -Rv -t ssh_home_t ~/.ssh
```

即可。

## 问题背景

由于实验需求，管理员分配给了我一台远程主机，并配置了新的用户和密码，ssh用密码登录没有问题，但是我尝试配置公钥登录时，添加了`~/.ssh/authorized_keys`的内容，却无论如何也不生效。

<!-- more -->

## 排查过程

### 1

检查相关文件的权限

```
$ ls -al ~/.ssh/
drwx------.  2 yuki users 4096 May  3 12:37 .
drwx------. 26 yuki users 4096 Apr 26 07:07 ..
-rw-------.  1 yuki users  112 May  3 12:38 authorized_keys
```

确定没有too open的问题。

### 2

使用`ssh -vvv`模式查看登录过程的debug，发现有对应私钥的验证过程，只是验证失败，说明本地SSH客户端配置无误。

### 3

远程主机执行`sudo /usr/sbin/sshd -d -p 22222`开启一个调试的SSH服务，发现可以通过22222端口正常使用公钥登录，说明SSH服务配置也无误。

### 4

修改远程`/etc/ssh/sshd_config`文件，添加一行

```
LogLevel Debug
```

然后执行`sudo systemctl restart sshd.service`重启SSH服务，再次登录后，执行`journalctl -u sshd.service`查看SSH服务输出，发现有如下记录：

```
debug1: Could not open authorized keys '/work0/yuki/.ssh/authorized_keys': Permission denied
```

基本确定是权限相关的问题。

### 5

检查sshd进程的权限

```
$ ps -ef | grep sshd
root     174675      1  0 May03 ?        00:00:00 /usr/sbin/sshd -D
```

确定是root身份。

### 6

注意到用户目录是在`/work0/`这种奇怪的路径下，可能是管理员怕磁盘空间不够，用奇怪的方式挂载了该目录，导致这一问题。经过一段时间查询，发现SELinux可能会使root被Permission denied。

通过如下命令确定问题

```
$ ls -alZ
drwx------. yuki users system_u:object_r:default_t:s0 .ssh
```

其中`ls -Z`是显示文件的[安全上下文](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/selinux_users_and_administrators_guide/sect-security-enhanced_linux-working_with_selinux-selinux_contexts_labeling_files)。注意到其中的`default_t`，这是普通文件的默认安全上下文，但不应用于`.ssh`目录。

### 7

尝试执行

```
$ restorecon -FRvv ~/.ssh
```

发现没有自动修改为正确的类型，于是执行

```
$ chcon -Rv -t ssh_home_t ~/.ssh
changing security context of ‘/work0/suzhihua/.ssh/authorized_keys’
changing security context of ‘/work0/suzhihua/.ssh’
```

手动修改为`ssh_home_t`。

### 8

再次登录，成功使用公钥，问题解决。