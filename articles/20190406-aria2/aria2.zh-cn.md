---
title: 编译一个在Windows下没有线程数限制的aria2
time: 2019-04-06T17:15:10+08:00
tags: [aria2]
categories: [技术]
toc: !!bool true
---
## 原理

基于官方提供的Dockerfile，在Docker中交叉编译。  
[https://github.com/aria2/aria2#cross-compiling-windows-binary](https://github.com/aria2/aria2#cross-compiling-windows-binary)

修改是基于aria2-fast的patch文件。  
[https://raw.githubusercontent.com/archlinuxcn/repo/master/archlinuxcn/aria2-fast/aria2-fast.patch](https://raw.githubusercontent.com/archlinuxcn/repo/master/archlinuxcn/aria2-fast/aria2-fast.patch)

<!-- more -->

## 过程

Windows下不太便于安装Docker，所以我是在Ubuntu虚拟机中安装了Docker进行操作的。

先将aria2的源代码clone下来，然后修改`Dockerfile.mingw`，修改之后如下，我把修改的地方用中文标注了。

```dockerfile
# Dockerfile to build aria2 Windows binary using ubuntu mingw-w64
# cross compiler chain.
#
# $ sudo docker build -t aria2-mingw - < Dockerfile.mingw
#
# After successful build, windows binary is located at
# /aria2/src/aria2c.exe.  You can copy the binary using following
# commands:
#
# $ id=$(sudo docker create aria2-mingw)
# $ sudo docker cp $id:/aria2/src/aria2c.exe <dest>
# $ sudo docker rm -v $id

FROM ubuntu:16.04

MAINTAINER Tatsuhiro Tsujikawa

# Change HOST to x86_64-w64-mingw32 to build 64-bit binary
### 编译64位程序 ###
ENV HOST x86_64-w64-mingw32

# It would be better to use nearest ubuntu archive mirror for faster
# downloads.
# RUN sed -ie 's/archive\.ubuntu/jp.archive.ubuntu/g' /etc/apt/sources.list
### 使用中科大ubuntu源 ###
RUN sed -i 's/archive.ubuntu.com/mirrors.ustc.edu.cn/g' /etc/apt/sources.list

RUN apt-get update && \
    apt-get install -y \
        make binutils autoconf automake autotools-dev libtool \
        pkg-config git curl dpkg-dev gcc-mingw-w64 \
        autopoint libcppunit-dev libxml2-dev libgcrypt11-dev lzip

### 使用代理下载文件 ###
RUN curl -x socks5h://192.168.31.5:1080 -L -O https://gmplib.org/download/gmp/gmp-6.1.2.tar.lz && \
    curl -x socks5h://192.168.31.5:1080 -L -O https://github.com/libexpat/libexpat/releases/download/R_2_2_5/expat-2.2.5.tar.bz2 && \
    curl -x socks5h://192.168.31.5:1080 -L -O https://www.sqlite.org/2018/sqlite-autoconf-3230100.tar.gz && \
    curl -x socks5h://192.168.31.5:1080 -L -O http://zlib.net/zlib-1.2.11.tar.gz && \
    curl -x socks5h://192.168.31.5:1080 -L -O https://c-ares.haxx.se/download/c-ares-1.14.0.tar.gz && \
    curl -x socks5h://192.168.31.5:1080 -L -O http://libssh2.org/download/libssh2-1.8.0.tar.gz && \
    curl -x socks5h://192.168.31.5:1080 -L -O https://raw.githubusercontent.com/archlinuxcn/repo/master/archlinuxcn/aria2-fast/aria2-fast.patch
### 上面加了一行，下载patch ###

RUN tar xf gmp-6.1.2.tar.lz && \
    cd gmp-6.1.2 && \
    ./configure \
        --disable-shared \
        --enable-static \
        --prefix=/usr/local/$HOST \
        --host=$HOST \
        --disable-cxx \
        --enable-fat \
        CFLAGS="-mtune=generic -O2 -g0" && \
    make install

RUN tar xf expat-2.2.5.tar.bz2 && \
    cd expat-2.2.5 && \
    ./configure \
        --disable-shared \
        --enable-static \
        --prefix=/usr/local/$HOST \
        --host=$HOST \
        --build=`dpkg-architecture -qDEB_BUILD_GNU_TYPE` && \
    make install

RUN tar xf sqlite-autoconf-3230100.tar.gz && \
    cd sqlite-autoconf-3230100 && \
    ./configure \
        --disable-shared \
        --enable-static \
        --prefix=/usr/local/$HOST \
        --host=$HOST \
        --build=`dpkg-architecture -qDEB_BUILD_GNU_TYPE` && \
    make install

RUN tar xf zlib-1.2.11.tar.gz && \
    cd zlib-1.2.11 && \
    CC=$HOST-gcc \
    AR=$HOST-ar \
    LD=$HOST-ld \
    RANLIB=$HOST-ranlib \
    STRIP=$HOST-strip \
    ./configure \
        --prefix=/usr/local/$HOST \
        --libdir=/usr/local/$HOST/lib \
        --includedir=/usr/local/$HOST/include \
        --static && \
    make install

RUN tar xf c-ares-1.14.0.tar.gz && \
    cd c-ares-1.14.0 && \
    ./configure \
        --disable-shared \
        --enable-static \
        --without-random \
        --prefix=/usr/local/$HOST \
        --host=$HOST \
        --build=`dpkg-architecture -qDEB_BUILD_GNU_TYPE` \
        LIBS="-lws2_32" && \
    make install

RUN tar xf libssh2-1.8.0.tar.gz && \
    cd libssh2-1.8.0 && \
    ./configure \
        --disable-shared \
        --enable-static \
        --prefix=/usr/local/$HOST \
        --host=$HOST \
        --build=`dpkg-architecture -qDEB_BUILD_GNU_TYPE` \
        --without-openssl \
        --with-wincng \
        LIBS="-lws2_32" && \
    make install
ADD https://api.github.com/repos/aria2/aria2/git/refs/heads/master version.json

### 执行一下patch再编译 ###
RUN git clone https://github.com/aria2/aria2 && \
    cd aria2 && patch -Np1 < ../aria2-fast.patch && autoreconf -i && ./mingw-config && make && \
    $HOST-strip src/aria2c.exe
```

编译和导出的方法在该文件最上面的注释中已经说明了，相信大家能看懂。

## 配置

最后附上我的aria2配置文件，速度非常快。

```properties
log=-
log-level=info
dir=E:\Downloads
input-file=aria2.session
save-session=aria2.session
max-concurrent-downloads=64
max-connection-per-server=32
check-certificate=true
enable-rpc=true
continue=true
split=4096
min-split-size=8M
piece-length=8M
check-integrity=true
retry-wait=5
max-tries=0
remote-time=true
disk-cache=512M
file-allocation=prealloc
```

注意因为配置了`input-file`，所以需要先在当前文件夹中建立一个空的`aria2.session`文件。

2019-10-13更新：

我将我之前编译的1.34.0版本放上来了，有需要可以下载：[aria2c.exe](assets/aria2c.exe)

最新编译的1.35.0，未经过很多测试：[aria2c-9d0a48a.exe](assets/aria2c-9d0a48a.exe)