---
title: 在Windows中利用ConEmu无缝使用Ubuntu+zsh
tags: [Windows, Ubuntu, ConEmu]
categories: [技术]
time: 2018-07-29T00:00:00+08:00
toc: !!bool true
autoNumber: !!bool true
---

## 在做什么？

不得不承认Windows在图形界面的应用性上远远高于其他操作系统，并且由于种种原因，我无法抛弃Windows。但是对于开发者来说，Linux下的很多功能是比Windows方便的，并且可能开发出来的应用需要在Linux下测试。为了获得一个兼具两者优势的环境，我尝试了很多方案，包括虚拟机、双系统等，但是无论是哪种方案都有一定的缺陷，例如虚拟机性能不够，双系统切换不便。

在大约一年前，微软在Windows 10上推出了WSL (Windows Subsystem for Linux)功能，即可以在Windows环境下使用Linux系统的一些功能。这一功能使得我们可以在一定程度上抛弃庞大的虚拟机，直接运行Linux程序。

我在这一功能刚刚推出的时候就开始尝试使用了。到目前为止，WSL技术已经基本成熟，可以作为一个稳定的工作环境了。因此，我探索了一下优雅使用这一功能的方法。

正如标题所说，我目前正在使用ConEmu定制命令行的显示效果，WSL的系统是Ubuntu 16.04，shell使用zsh。这篇文章讲述的就是如何搭建这样一个环境。

## 效果

![右键菜单](assets/context.png)

![界面](assets/console.png)

<!-- more -->

## 所需环境

Windows 10 Professional / Enterprise

## 让我们开始吧

### 第一步 启用WSL功能

进入“设置”→“更新和安全”→“针对开发人员”→选择“开发人员模式”→重启。

![选择“开发人员模式”](assets/开发人员模式.png)

进入“控制面板”→“程序和功能”→“启用或关闭Windows功能”→勾选“适用于Linux的Windows子系统”→重启。

![勾选“适用于Linux的Windows子系统”](assets/适用于Linux的Windows子系统.png)

### 第二步 安装Ubuntu系统

打开Microsoft Store，搜索“Ubuntu”，或直接打开链接[安装Ubuntu](https://www.microsoft.com/store/productId/9NBLGGH4MSV6)。

*如果以前使用过WSL的话，应该注意到这一步与以前安装子系统的方法有所不同。这是因为微软现在采用Microsoft Store来分发不同的WSL。(可以用同样的方法安装kali Linux*

打开开始菜单看看，现在应该有一个类似UWP程序的Ubuntu的入口了。打开它，它会自动开始安装完整的文件并引导你初步配置系统。

安装之后重启。

### 第三步 安装zsh，配置Ubuntu

既然大家都在尝试使用WSL了，那么Linux的操作应该是不成问题的，所以这一步就不多说了，按照一般在Ubuntu系统中的操作方法安装即可。

可以参考[这篇博客](https://www.jianshu.com/p/546effd99c35)或[这篇博客](https://www.jianshu.com/p/9a5c4cb0452d)。

### 第四步 安装ConEmu

[ConEmu - Handy Windows Terminal](https://conemu.github.io/)

正常安装就好。

### 第五步 配置ConEmu

启动ConEmu，打开Settings（Win+Alt+P），进入Startup->Tasks，添加一项“Ubuntu::zsh”，Commands的大框中输入

```
bash -c zsh -cur_console:p
```

如果你有兴趣的话，可以在[Ubuntu design](https://design.ubuntu.com/downloads/)上下载一个Ubuntu的图标，然后在Task parameters中指定它。

最后Tasks配置如图：

![Tasks配置截图](assets/tasks.png)

然后在Startup中指定ConEmu的启动任务：

![Startup配置截图](assets/startup.png)

配置完成后，点击Save settings。

### 最后配置右键菜单项

打开ConEmu，Settings，左侧选择“Integration”。

添加一项ConEmu Here的设置，参考下面的截图：

![Integration设置](assets/integration.png)

**设置好之后点击“Register”按钮**，这时在文件管理器中右键就应该可以看到“Ubuntu zsh Here”的菜单项了。最后点击Save settings。

## 测试

在一个文件夹中点右键，选“Ubuntu zsh Here”，应该可以直接进入当前目录，通过zsh shell操作。

Great Job!