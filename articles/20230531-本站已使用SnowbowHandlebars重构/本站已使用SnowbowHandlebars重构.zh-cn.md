---
title: 本站已使用SnowbowHandlebars重构
time: 2023-05-31T21:50:00+08:00
categories: [技术]
tags: [SnowbowHandlebars]
---
很高兴地宣布，本站已经使用[SnowbowHandlebars](https://github.com/Yuki-Nagato/SnowbowHandlebars)重构。这是一个基于Handlebars的静态博客生成引擎（static site generator），它的特点是：

- 良好的i18n支持；
- 良好的pandoc支持；
- 良好的handlebars模板支持。

<!-- more -->

相对于前代Snowbow，SnowbowHandlebars抛弃了Razor的历史包袱，使用了更加符合前端习惯的Handlebars模板语言，便于前端工程师开发新的主题。同时支持了.NET 6。

在i18n方面，比前代提供了更细粒度的语言优先级控制，支持文章使用除了默认语言之外的其他语言优先渲染。

与此同时，随着浏览器阅读模式的不断完善，RSS/Atom的价值越来越低，因此暂时放弃了对RSS/Atom的支持。不过之后有时间的话还是会加上的。

SnowbowHandlebars的源码已经开源，欢迎大家使用和反馈，地址：<https://github.com/Yuki-Nagato/SnowbowHandlebars>。本站的主题lightsnow也已经开源，地址：<https://gitlab.com/Yuki-Nagato/yuki-nagato.gitlab.io>。
