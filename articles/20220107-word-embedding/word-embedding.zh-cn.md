---
title: 静态词嵌入（Static Word Embedding）总结
time: 2022-01-07T14:50:00+08:00
categories: [研究]
tags: [NLP,word embedding]
toc: !!bool true
autoNumber: !!bool true
---
大家好，欢迎来到2022（twenty-twenty too 😉）。

研究生的这一年多来还是挺难过的，疫情是一方面，另外主要是我研一的时候课题组除了导师以外只有我一个学生，没有师兄带我，和其他人的交流也很少。研二的时候新来了一个同学，不过我还没怎么见过他，不像我在新生的时候就开始跟导师读论文写综述了。现在想想，如果我在研一的时候也像他那样，多花精力在课程和生活上，而不是在读论文和写综述上，可能我能更好地适应研究生的生活，少一些痛苦。虽然有人可能觉得研一就进入科研状态挺好的，毕竟积累得早收获得多，但是每周都要写几页综述还要做组会PPT确实给我带来了很多压力，影响了精神状态。

但总之我也确实积累了一些材料，主要是读论文的总结，打算最近整理一下发在博客上。本篇关于静态词嵌入的总结，是我在研一上学期时导师带着写的，每周导师会给我发5篇左右的论文，我阅读之后整理主要内容并写成综述和PPT。网页中是综述的内容，[PPT可以点击此处下载](assets/static_word_embedding.pptx)。由于我刚接触该领域，可能会有些写得不对的地方，请多批评指正。我也会尽量标注参考文献（目前是ACL会议格式，后续可能会改为链接），如果对某些内容有兴趣，请以原文为准。

以下是正文。

## 前言

本文将若干篇词嵌入相关的论文组合起来，对词嵌入在各个阶段和层次的发展过程进行了梳理，整理了词嵌入相关的扩展方法和评价方法。

<!-- more -->

## 词嵌入的原理

词嵌入的含义是用较低维（相对于词汇表大小来说）的向量表示词语的含义（语法和语义），这里的“嵌入”一词表示将高维的对象投射到低维空间中。有了这个想法后，关于词嵌入的研究首先就面临着两个问题，即词嵌入应该怎样计算和词嵌入应该有怎样的性质。

从历史上看，研究工作通常都是先依据需求来寻找解决方法，然后再发现解决方法中的特点，也就是说，性质应该是模型的设计目标。不过本文作为一篇综述类的文章，还是先从模型和构造方法入手，再去讨论性质和评价方法。

在语言学研究中，一个对当今基于统计的语言处理方法最有用的假设就是分布假设，即“上下文相似的词，其语义也相似” (Harris, 1954)。后来这一假设被进一步具体化，即“词的语义由其上下文决定” (Firth, 1957)。许多词嵌入模型都是基于这个假设，利用词语的上下文信息构造词嵌入。

另一个关于词嵌入的重要基础是语言模型，它是从句子的角度对语言建模的，要求输入一个字词串，输出该字词串出现在自然语言的概率。为了实现该模型，常常会引入马尔科夫假设，用n-gram模型来估算语言模型。语言模型与词嵌入模型的关联在于，如果我们明确定义了语言模型的概率函数的计算方法，也有了字词串的实际概率，那么理论上就可以求解出每个词的数学表示，该表示就可以作为一种词嵌入。

## 词嵌入表示的构造学习

基于不同的思想（主要是上节所述的分布假设和语言模型），产生了许多词嵌入的计算方法。

### 基于语言模型的学习

上文提到，语言模型是一种概率估计，传统的实现方法是引入马尔科夫假设，用连续的n-gram模型概率相乘得到字词串的概率。设一个字词串$W = \left( w_{1},\ w_{2},\ \ldots,\ w_{T} \right)$，它的概率可以表示为

$$p(W) = p\left( w_{1},w_{2},\ldots,w_{T} \right) = p\left( w_{1} \right)p\left( w_{2} \middle| w_{1} \right)p\left( w_{3} \middle| w_{1}w_{2} \right)\ldots p\left( w_{T} \middle| w_{1}\ldots w_{T - 1} \right)$$

但是这个算法中$p\left( w_{k} \middle| w_{1}\ldots w_{k - 1} \right)$涉及到$V^{k}$个参数，其中V是词典大小，如果字词串长度较大就无法接受。N-gram模型可以减少参数量，它假设

$$p\left( w_{k} \middle| w_{1}\ldots w_{k - 1} \right) \approx p\left( w_{k} \middle| w_{k - n + 1}\ldots w_{k - 1} \right)$$

即用前n-1个词可以替代前k-1个词预测下一个词的概率，这样参数量就降低到了$V^{n}$，n如果较小的话（不大于3）还是可以接受的。

但是简单统计的n-gram方法也有问题，就是$V^{n}$个参数太稀疏了，如果在实际计算过程中遇到一个单词组合是语料库中没有的，那么整个概率就会变成0，这不利于概率计算。为了解决这个问题，一种方法是做平滑处理，让没有出现过的单词组合也有一个较小的概率。另一种方法就是更充分地利用单词的信息，语法和语义上相似的单词互相替换后应该在语言模型计算后获得相似的概率，下面介绍的神经语言模型就用了这种想法。

#### 神经语言模型

神经语言模型（NNLM）通过神经网络实现语言模型，达到了同时训练语言模型和词嵌入的效果 (Bengio et al., 2003)。它将每个单词都映射到一个向量，将几个单词输入到模型中后尝试预测下一个单词，模型的训练目标就是让预测的结果接近真实的概率。

这篇论文的创新点可以概括为三个方面：

第一是用神经网络实现了语言模型。在此之前，语言模型常常需要大量的统计和存储，通过n-gram的方法来估算一个词序列在语言中是正常句子的概率，即$P\left( w_{1}^{T} \right) = \prod_{}^{}{P\left( w_{t} \middle| w_{1}^{t - 1} \right)} \approx \prod_{}^{}{P\left( w_{t} \middle| w_{t - n + 1}^{t - 1} \right)}$。由于复杂度的限制，这种方式一般最多只能做到$n = 3$也就是trigram的大小。NNLM的想法是，上面的概率函数P不再使用统计的方法实现，而是使用神经网络。这个神经网络通过学习，实现输入一个序列，输出预测下一个词概率的功能，即$f\left( w_{t},\ \ldots,\ w_{t - n + 1} \right) = P\left( w_{t} \middle| w_{1}^{t - 1} \right)$，自然其中的限制是$\sum_{i = 1}^{|V|}{f\left( i,\ w_{t - 1},\ldots,\ w_{t - n + 1} \right)} = 1,\ f > 0$，所以使用$|V|$维的softmax作为输出，这也是算法中时间复杂度的瓶颈。

第二是将词的向量表示作为训练参数。因为相似的词语在语料库中有相似的语境，所以通过初始化一个word embedding矩阵，每一行是一个单词的向量表示，也就是词嵌入，词嵌入也是训练参数，在每次训练中进行更新，最后得到的结果可以使相似的词语有相似的表示。这里可以看出词嵌入是语言模型的一个附属品，但后来的研究发现，词嵌入本身也是一个很好的工具。

第三是尝试将训练过程并行化处理。分为两种情况，一种是在共享内存处理器的条件下，进行数据的并行处理，每个处理器工作在不同的数据子集。每个处理器计算它拥有的训练样例的梯度，执行随机梯度下降算法更新内存中共享的参数。每个处理器可以在任意时间向共享的内存中写数据，有时会产生一些写冲突而丢失数据，这导致了参数更新的一些噪声，然而这种噪声是很微不足道的。另一种是在CPU的网络的情况下，进行参数的并行处理。每个CPU计算一块输出节点的梯度，然后在一个CPU上求和并共享总梯度，再由每个CPU分块进行梯度下降。

当模型训练完整后，每个单词映射的向量就是词嵌入表示。

#### C&W模型

NNLM (Bengio et al., 2003) 使用神经网络实现了语言模型和词嵌入的同时训练，C&W模型 (Collobert and Weston, 2008) 同样采用了神经网络架构，但是他们考虑了更多的词语特征和语言处理任务，并证明同时训练这些任务可以有效提升性能。

在该文中，他们首先列举了六项NLP任务，分别是词性标记（POS）、分块（浅层分析，chunking）、命名实体识别（NER）、语义角色标记（SRL）、语言模型和语义相关单词（同义词）。他们认为这些任务中SRL是最复杂的，所以将这项任务的性能作为主要评价指标，其他任务用于辅助提升SRL的性能。

然后他们描述了该模型的通用架构。该模型可以分为三层，第一层是将单词和它的特征（如大小写）映射到向量，单词和每种特征都分别对应一个lookup table，其中单词的lookup table有\|D\|列，特征的lookup table的列数等于该特征的类数（如是否大小写可以看作true和false两类，该lookup table就有两列），这些向量是可以通过反向传播训练的。第二层是卷积和池化，然后可以接一个可选的非线性层。第三层是线性层和softmax。

上述六个任务中，前四个任务（POS、chunking、NER、SRL）都可以直接看作词级别的多分类任务，所以可以直接使用这个架构，利用有标注的数据进行有监督学习。语言模型任务是通过设计转化为了一个词序列的二分类任务，即把模型的输出层的softmax改为输出一个打分，分数越高表示该序列越符合语言模型，最后目标是最小化$\sum_{s \in S}^{}{\sum_{w \in D}^{}{\max\left( 0,\ 1 - f(s) + f\left( s^{w} \right) \right)}}$，其中s是语料库中的词序列，$s^{w}$表示把s中间的词替换为了w，这样$f(s)$的值就应该较大，$f\left( s^{w} \right)$的值就应该较小。这种方式将传统语言模型的预测任务转化为了判别任务，大大降低了输出层节点的数量，也避免了softmax复杂度高的问题。

实验过程是，首先用语言模型训练lookup table，然后用几个单词的近邻词定性地说明了语言模型产生的词嵌入效果较好。这个lookup table继续由多任务学习共用，相当于用其他任务来微调语言模型得到的词嵌入表示，并测试了若干个任务组合训练后SRL的错误率，结果证明多任务学习确实有助于降低SRL的错误率，其中SRL+语言模型的半监督多任务学习的效果最好。

#### Global Context-Aware Neural Language Model

Huang et al. (2012) 提出了这一模型，这个模型借鉴了C&W模型中语言模型的设计，在它的基础上增加了全局文本的信息，目标函数为最小化

$$C_{s,d} = \sum_{w \in V}^{}{\max\left( 0,1 - g(s,d) + g\left( s^{w},d \right) \right)}$$

其中s是词序列，d是完整文本，$g(s,d)$为得分函数，$g\left( s^{w},d \right)$为s序列中最后一个词由w替代后的得分。同时，需要把两者差值控制在\[0, 1\]范围之内。

得分函数g的实现分为两部分，第一部分与C&W类似，对词嵌入序列进行非线性变换，第二部分是把文档嵌入和最后一个词的嵌入拼接起来，然后进行非线性变换。其中文档嵌入的计算方法就是将文档中所有的词做加权平均。

这篇论文还有一部分是关于学习词语的多原型表示，这部分将在“多义词的表示方法”一节介绍。

这篇论文的实验部分可以分为四个实验，分别是Global Context-Aware Neural Language Model的定性和定量评估，以及Multi-Prototype Neural Language Model的定性和定量评估。因为这个模型是2012年提出的，所以还没有word2vec和GloVe等词嵌入模型，这篇论文主要的对比SOTA对象是上一节介绍的C&W模型。其中关于Global Context-Aware Neural Language Model的定性评估方法是选择三个与特定词语距离最近的词，看词义相似度；定量评估方法是在WordSim353数据集上算相关系数，结果显示该模型在不使用stop words的情况下效果最好。

#### Word2vec

Word2vec (Mikolov et al., 2013a) 总结并简化了NNLM和C&W模型，只保留了根据某个单词预测上下文单词（或相反）的部分。在这篇论文中，介绍了word2vec的两个模型，分别是Continuous Bag-of-Words Model (CBOW) 和Continuous Skip-gram Model (skip-gram)，两个模型具有相同的结构，只是输入输出不同，前者是输入多个上下文单词，输出目标词的概率，后者是输入一个目标词，输出上下文单词的概率。

与NNLM类似，word2vec也使用了神经网络的结构，但是不含有非线性层，只有线性结构，用两个向量的内积来表示两个单词共同出现的概率。这篇论文中没有过多提及模型结构的具体定义，后来该算法渐渐被其他人所分析和解释 (Goldberg and Levy, 2014)，具体的模型目标函数将在“词嵌入模型的关联”一节讨论。

相对的，这篇论文中做了很多关于复杂度的对比分析，他们指出，对于下面提到的所有模型，训练复杂度正比于O=E×T×Q，其中，E是指训练的迭代次数，T是训练集中词的数量，Q是一个样本的训练复杂度，由模型定义。E一般的选择范围是3到50，T可达到1e9的数量级。

- 对于NNLM，一个样本的训练复杂度是Q=N×D+N×D×H+H×V，其中N是输入的词数量（n-gram），D是投影层的维度，H是隐含层维度，V是词表大小。

- 对于RNNLM，一个样本的训练复杂度是Q=H×H+H×V。

- 对于CBOW，文中给出了它使用了二叉树优化的softmax的复杂度，Q=N×D+D×log(V)。

- 对于Skip-gram，同样是使用了二叉树优化的softmax的复杂度，Q=C×(D+D×log(V))，其中C是窗口大小。

在这项工作中，发现word2vec生成的词嵌入具有语义线性运算（analogy）的性质，一个经典的例子是king - man + woman = queen。能否实现analogy也成为了评价词嵌入的主要指标之一，具有这种语义关系的词嵌入可用于改进许多现有的自然语言处理应用程序，如机器翻译、信息检索和问题回答系统。论文中也测试了传统的语义相关性性能，如意思相近的词挨得比较近，而且每个单词可以具有多个相似度。具体的词嵌入评价方法和结果将在“词嵌入的性质和评价”一节讨论。

##### 词嵌入模型运算优化

许多词嵌入模型的输出都是一个概率，如word2vec就是输出目标词或上下文词的概率，而多分类问题的概率常常会使用softmax来表示。但是对于词嵌入模型来说，每次迭代softmax需要O(V)的复杂度计算，其中V是词表大小。这个复杂度太大不太能接受，所以可以尝试以下两种方法优化softmax的复杂度，hierarchical softmax和negative sampling。这两个方法被word2vec引用并作为主要的训练方法(Mikolov et al., 2013b)。

###### Hierarchical Softmax

作为一种计算高效的近似方法，hierarchical softmax被广泛使用。Morin and Bengio (2005) 首先将这种方法引入神经网络语言模型，后来由Mnih and Hinton (2009) 改良，由Mikolov et al. (2013b) 引入skip-gram模型。该方法不用为了获得概率分布而评估神经网络中的W个输出结点，而只需要评估大约log(W)个结点。层次softmax使用一种完全二叉树结构来表示词典里的所有词，V个词都是二叉树的叶子结点，而这棵树一共有V−1个非叶子结点。

对于每个叶子结点（词），总有一条从根结点出发到该结点的唯一路径。这个路径很重要，因为要靠它来估算这个词出现的概率。以下图为例，白色结点为词典中的词，深色是非叶子结点。图中画出了从根结点到词w2的路径，路径长度L(w2)=4。n(w, j)表示从根结点到词w2的路径上的的第j个结点。

在模型的训练过程中，通过Huffman编码 (Huffman, 1952)，构造了一颗庞大的Huffman树。我们要计算的是目标词w的概率，这个概率的具体含义，是指从root结点开始随机走，走到目标词w的概率。因此在途中路过非叶子结点（包括root）时，需要分别知道往左走和往右走的概率。例如到达非叶子节点n的时候往左边走和往右边走的概率分别是：

$$p(n,left) = \sigma\left( h_{n} \right)$$

$$p(n,right) = 1 - \sigma\left( h_{n} \right) = \sigma\left( - h_{n} \right)$$

容易证明所有输出节点的概率和为1。

###### Negative Sampling

对于一组输入输出来说，模型的输出结果只有一个理论值为1的正例，其他都是值为0的负例。所有的这些权重需要通过反向传播进行调整，这是非常消耗计算资源的。除了上面介绍的hierarchical softmax，也可以使用negative sampling解决这个问题 (Mikolov et al., 2013b)。该方法来源于noise-contrastive estimation (NCE)，即通过人工噪声训练可以近似估计对数概率，后来该方法被用于训练词嵌入 (Mnih and Kavukcuoglu, 2013)，并被skip-gram模型采用。

当使用negative sampling时，将随机选择一小部分的negative words（比如选5个negative words）来更新对应的权重。当然原本输出为1的节点也应计入并更新对应权重。也就是说，从原本的V个输出层节点权重全部更新，改为仅更新6个节点的权重，这样计算效率就大幅提高了。

一个问题是如何选择negative words。因为在训练过程中，出现频率更高的词作为正样例的次数更多，所以这些节点的权重提升更充分。因此，一种相对的合理想法是，在这些节点不作为正样例的情况下，进行更多打压才有助于修正模型。所以negative words也是根据他们出现概率来选的，而这个概率又和他们出现的频率有关。更常出现的词，更容易被选为negative words。通过测试，一个表现比较好的负采样概率是：

$$P\left( w_{i} \right) = \frac{U\left( w_{i} \right)^{0.75}}{\sum_{j}^{}{U\left( w_{j} \right)^{0.75}}}$$

其中U(w)表示词w的unigram概率，也就是词频。

### 基于共现矩阵分解

上面所述的方法都是基于神经网络的，确实神经网络有它的长处，比如增加数据集的时候只要在原来的基础上继续训练，可尝试的超参数比较多，容易达到较高的性能。但另外也有一些基于统计和数学的方法，这类方法的特点是易于推导和解释，在实现上也易于调试。

为了实现基于统计和数学的词嵌入方法，首先引入向量空间模型（vector space model, VSM）的概念，这个模型的核心单词分布的统计，也就是共现矩阵。常用的共现矩阵统计方法可以分为三种：term-document、word-context和pair-pattern (Turney and Pantel, 2010)：

-   term-document: 行向量对应于一个term，通常是一个词；列向量是一个document，例如一个网页。$x_{i,j}$表示词i在文档j中的出现次数。

-   word-context: 行向量是词，列向量是上下文（如前后几个词、短语、句子、段落、章节或文档等）。$x_{i,j}$表示词i在上下文j中的出现次数。

-   pair-pattern: 行向量是词对，列向量是词对的模式。如“carpenter cuts wood”在矩阵$x_{i,j}$中，i是“carpenter : wood”，j是“X cuts Y”。

共现矩阵可能需要进一步处理，比如加权、平滑等。一种比较好的平滑方式是truncated Singular Value Decomposition (truncated SVD)。SVD是一种矩阵分解方法，原理是任意一个m\*n的矩阵X，都可以把它分解为三个矩阵的乘积$X = USV^{T}$。设$q = \min(m,n)$，则三个矩阵的大小分别为U (m\*q)，S (q\*q)，V (n\*q)，且S是一个对角矩阵，对角线上的值是奇异值。Truncated是指SVD分解后，取S中top k的奇异值，和U、V中对应的k列，可以构成原共现矩阵的近似$\widehat{X} = U_{k}\Sigma_{k}V_{k}^{T}$。这样做的好处在于可以降低X的稀疏度，加强上下文的关联性，并且X的行空间和列空间都映射到了k维空间中。对于词嵌入任务，可以将$U_{k}\left( \Sigma_{k} \right)^{\alpha}$的每行作为词语表示。

#### 非负稀疏嵌入（NNSE）算法

Murphy et al. (2012) 将非负稀疏编码（NNSC）的方法应用在了生成词嵌入上，它的原理是将共现统计矩阵X尽可能分解为A\*D，其中A是一个非负稀疏矩阵，大小为m\*k，每一行作为词嵌入表示。

这篇论文的实验部分占较大篇幅，他们首先说明了共现统计采用的是正点逐互信息（PPMI），测试中NNSE算法并不是直接分解PPMI矩阵，而是先用SVD分解降维再使用NNSE，这样可以提高运算效率。然后列举了多个模型的实验结果，其中最好的是组合的SVD算法和组合的NNSE算法，“组合”是指共现计数同时包含了依赖计数（dependency counts）和文档共现计数（document co-occurrence counts）。

接下来是对SVD算法和NNSE算法的详细比较。分为三个实验，第一个实验是神经语义解码，通过采集测试者大脑的磁共振成像，获得特定概念在大脑中的激活点，然后看词嵌入能否与这些点线性对应起来，结果是在词嵌入维度较低的情况下SVD的表现更好，但是在维度较高的情况下SVD和NNSE的表现差不多。第二个实验是评估稀疏度，这个很显然SVD的结果是稠密的，而NNSE显著稀疏。第三个实验是评估可解释性，这里的可解释性指的是词嵌入的每个维度是否分别有特定的含义，方法是词语入侵检测。词语入侵检测数据的构造方法是，将词嵌入按照一个特定的维度排序，这个维度的值大的词排在前面，小的排在后面，然后取出排在前五的五个词和一个排在后半部分的词（入侵词），将这六个词放在一起随机打乱顺序，然后由测试者尝试挑出这六个词中的入侵词，例如一个测试数据是{bathroom, closet, attic, balcony, quickly, toilet}，其中quickly是入侵词。最后比较SVD和NNSE在这项任务的准确率，结果是NNSE算法的可解释性远高于SVD。

#### GloVe

GloVe (Pennington et al., 2014) 的本质是利用共现矩阵来确定词语的相关度，该模型同样采用了类似矩阵分解的方式，让两个词嵌入的内积接近log共现次数。与SVD和NNSE的区别在于，GloVe在训练的过程中，可以对共现矩阵中的非零元素进行随机采样，而零元素可以忽略，以达到加速的目的。

论文首先进行了目标函数的推导，首先从单词共现频率的统计结论出发，举例说明了共现频率可以提取词语某些方面的相关性信息。然后，变换预想函数的形式，从而每两个词嵌入的内积将尽可能接近其log共现计数。最后，他们提出了一个加权最小二乘回归模型，并引入了一个加权函数，以便每个单词对函数的贡献与其频率有关，而频率过高的单词不会对目标函数造成太大干扰。

论文中也对该模型的复杂度进行了分析，GloVe模型的算法复杂度取决于共现矩阵中的非零元素的个数，最坏的情况下为$O\left( V^{2} \right)$。由于词汇表的数量通常很庞大，因此$V^{2}$会非常大。但是实际上单词的共现情况满足一定的分布假设（$X_{\text{ij}} = \frac{k}{\left( r_{\text{ij}} \right)^{\alpha}}$），所以算法复杂度较低，约为$O\left( |C| \right)$，其中C为语料库大小。

论文最后是对模型的评价，评估方法包括word analogy、word similarity和named entity recognition。具体的词嵌入评价方法和结果将在“词嵌入的性质和评价”一节讨论。

#### Canonical Correlation Analysis (CCA)

典型相关分析（Canonical Correlation Analysis, CCA）是一种统计学的分析方法，它的目标是找出两个高维随机变量$X = \left( X_{1},\ X_{2},\ \ldots,\ X_{n} \right),\ Y = \left( Y_{1},\ Y_{2},\ \ldots,\ Y_{m} \right)$的相关性，更具体地说是找到两个线性变换$a,\ b$，使得两个随机标量$U = a^{T}X,\ V = b^{T}Y$的相关系数最大。具体算法是用SVD分解$M = S_{\text{XX}}^{- \frac{1}{2}}S_{\text{XY}}S_{\text{YY}}^{- \frac{1}{2}}$，其中S表示方差和协方差，然后得到最大的奇异值$\rho$，和最大奇异值对应的左右奇异向量$u,v$，那么就可求得$a = S_{\text{XX}}^{- \frac{1}{2}}u,\ b = S_{\text{YY}}^{- \frac{1}{2}}v$。

Stratos et al. (2015) 将CCA用于推导词嵌入模型。他们的想法是，如果用one-hot表示单词，每个目标词看成随机变量X，每个上下文词看成随机变量Y，那么语料库中的每对共现词就是随机变量(X, Y)的样本。我们可以对这些样本进行CCA分析，得到使X和Y相关性最大的变换。上面提到CCA的算法是用SVD分解一个矩阵，经过推导，这里实际上要分解的矩阵就是

$${\widehat{\Omega}}_{w,c}^{\langle a\rangle} = \frac{{\#(w,c)}^{a}}{\sqrt{{\#(w)}^{a}{\#(c)}^{a}}}$$

最后还是取$U\Sigma^{\beta}$作为词的表示。

论文中还把各种矩阵分解的词嵌入算法（spectral methods）都归结成了一个模板，这些算法都以共现计数为基础，经过不同的数据转换（transformation）和量纲（scale）调整，然后进行低秩的SVD分解，最后取$U\Sigma^{\beta}$作为词嵌入。他们尝试了这些操作的多种组合，并与word2vec和GloVe模型做了对比，结果大致是：(sqrt transform + cca scaling)的组合在similarity任务上表现最好，skip-gram模型在analogy任务上表现最好，(sqrt transform + cca scaling)的组合在低维度下NER的表现最好，(log transform + no scaling)的组合在高维度下NER的表现最好。

#### 随机游走模型

Hashimoto et al. (2016) 将语言生成的过程看做是词嵌入的随机游走，从词汇i走到词汇j的概率与词嵌入在空间中的距离的对数似然成正相关。该工作的贡献可以总结为以下四点：

-   将大型语料库中单词的log共现率与语义相似性评估联系起来，并表明共现率确实符合欧氏语义空间假说。首先，作者证明了词语共现的log PMI值与人类语义相似度判断是成正比的，然后作者用PMI值计算了geometric sampling (GS)模型的参数，从心理测量学角度证明了共现次数确实可以在欧氏空间中表达词语含义。

-   提出了一个随机游走模型，将语料库视为词语在欧氏空间的随机游走，每个词语产生的概率与上一个词相关，并证明了在这种模型下，当语料库和词汇表大小趋于无穷时，每对词的共现次数会趋近于$\left\| x_{i} - x_{j} \right\|^{2}$。

-   这个随机游走模型也为流形学习(manifold learning)提供了思路，即如果要获得一组高维向量的低维嵌入（不仅是词嵌入，也可以是图嵌入等），只要把这些高维向量连成邻接图，然后在邻接图上随机游走生成“句子”，然后在这些“句子”上用词嵌入算法即可得到低维嵌入。

-   基于随机游走模型，提出了一个词嵌入的计算方法，即共现矩阵中的每个值应该符合负二项分布，令它的期望和标准差用词嵌入的距离表示，即可得到一个最小化对数似然的模型，作者将该模型称为度量回归模型（metric regression model）。

这篇论文的实验部分包括两部分，第一是对词嵌入效果的评价，评价指标包括analogy、sequence completion和classification，与GloVe、SVD和word2vec进行了对比，结果显示该模型（regression）和word2vec在各项测试中表现最好。第二是测试了使用词嵌入模型进行流形学习的效果，方法是在MNIST手写数字数据集上使用随机游走模型生成语料库，然后用不同的词嵌入方法（regression、GloVe、SVD和word2vec）生成二维的向量，并与四种标准的降维方法进行了对比（PCA、Isomap、SNE和t-SNE），结果显示regression的效果很好，仅次于t-SNE并优于SNE。

Arora et al. (2016) 的随机游走模型比Hashimoto et al. (2016) 的更泛化了一些，他是一个语义变量discourse vector的运动，每次运动都会产生一个词，选择每个词的概率与exp语义向量和词嵌入的内积成正比。在这个模型下，可以推导出词共现的概率、词频、PMI与词嵌入的关系。

在时刻t，假设有随机移动的语义变量$c_{t}$，单词$w_{t}$根据下式产生：

$$p\left( w_{t} \middle| c_{t} \right) = \frac{\exp\left( w_{t}^{T}c_{t} \right)}{\sum_{w}^{}{\exp\left( w^{T}c_{t} \right)}}$$

有的单词概率非常大，它反映在单词嵌入w的范数上，即$w = s \cdot \widehat{w}$，其中$\widehat{w}$是单位球面C上的向量，s是概率变量。

该模型可以推导出共现概率PMI与词嵌入的关系

$$\log{p_{q}(v,w)} = \frac{|v + w|^{2}}{2d} - 2\log Z + \log\frac{q(q - 1)}{2} \pm \epsilon$$

$$\text{PMI}_{q}(v,w) = \log\frac{p(v,w)}{p(v)p(w)} = \frac{v^{T}w}{d} + \log\frac{q(q - 1)}{2} + O(\epsilon)$$

其中q是窗口大小，d是词嵌入和语义向量的维度。这个推导过程基于全概率公式。

基于这两个关系，文中推导出了一个Squared Norm模型，并将该模型的目标函数与GloVe和CBOW的目标函数进行了对比，发现就是将GloVe和CBOW中的一些训练参数赋予了实际含义，具体地说，就是GloVe中的偏置量$s_{w} = |w|^{2}$，CBOW中的上下文词嵌入的均值$\frac{1}{K}\sum_{k = 1}^{K}w_{k}$是语义变量$c_{t}$的最大似然估计。

这篇论文中还解释了analogy现象（即RELATIONS=LINES）的原因：假设我们有一些关系R（例如man-\>king的关系），如果我们有两个词a和b满足这个关系，则我们有：

$$V\left( v_{a} - v_{b} \right) = d\log\left( v_{R} \right) + \zeta_{a,b,R}^{'}$$

当词嵌入的维度较低时，噪声项$\zeta_{a,b,R}^{'}$会减小，此时向量差就可以表示关系R。

论文中的实验分为三部分，第一部分验证了文中推导的一些结论，如partition function的分布情况、奇异值的各向同性、词频与词嵌入长度的线性关系。第二部分是测试词嵌入在analogy任务上的性能，结论是与GloVe和word2vec相当。第三部分是测试RELATIONS=LINES的关系方向（RD）是否存在，通过奇异值运算验证了这一点，并利用RD优化了analogy的性能。

### 词嵌入模型的关联

上面介绍的词嵌入模型虽然目标和方法不同，但是在相应的论文中或后来也有人讨论过这些模型的关联，我们在这里描述一下。

#### 向量内积与向量的和、差的平方

在模型的目标函数和推导过程中常常会出现两个向量的内积、和的平方、差的平方，这三者形式不同，但是实际上只相差系数和bias，即

$$\left| v_{1} + v_{2} \right|^{2} = \left( v_{1} + v_{2} \right)^{2} = 2v_{1} \cdot v_{2} + v_{1}^{2} + v_{2}^{2}$$

$$\left| v_{1} - v_{2} \right|^{2} = \left( v_{1} - v_{2} \right)^{2} = - 2v_{1} \cdot v_{2} + v_{1}^{2} + v_{2}^{2}$$

其中$v_{1}^{2}$和$v_{2}^{2}$可以分别看成是与$v_{1}$和$v_{2}$相关的bias。

#### 一些相关性的结论

有些论文中分析了它的模型与一些常见模型的关系，还有一些专门分析相关性的研究，这里列举一些结论。

一个很经典的结论就是，skip-gram + negative sampling (SGNS) 是对PMI矩阵分解的一种可能的逼近 (Levy et al., 2014)，即

$$\overrightarrow{w_{i}} \cdot \overrightarrow{c_{j}} = PMI\left( w_{i},c_{j} \right) - \log k$$

其中k是负采样的个数。也就是说，如果目标词嵌入w和上下文词嵌入c的维度都足够大，当负采样的个数为1时，两个词嵌入的乘积就近似于这两个词的PMI，当负采样的个数大于1时，两个词嵌入的乘积就近似于这两个词的PMI减去一个偏移量。

这个结论也启发我们或许可以直接将PMI矩阵或PMI矩阵的某种分解作为一种词嵌入。因为PMI矩阵本身有缺陷（稠密、有负无穷大值），所以需要先做一个近似变换，论文中提出了SPPMI的概念

$$\text{SPPMI}_{k}(w,c) = \max\left( \text{PMI}(w,c) - \log k,0 \right)$$

至于分解方法，论文中尝试了SVD，并对比了它与SGNS在不同任务上的效果。实际上，最后的结果表明，SPPMI确实能比较好地近似PMI，也就是说它能较好地优化目标函数。但是在分解后，SVD方法在k较大的情况优化程度不如SGNS。在similarity和analogy两个语言任务上，直接使用SPPMI或用SVD分解SPPMI表现也不一定比SGNS更好，作者认为这可能是因为SGNS能重点优化高频词的表示，忽略少量低频词对模型的影响，而SVD对矩阵中每个值的处理权重是相同的。

还有一个结论，Latent Variable Model (Squared Norm, SN) (Arora et al., 2016) 的目标函数与GloVe是相似的，仅系数、偏置和高阶项有差别。

#### 目标函数总结

这里将多篇论文的模型和目标函数列举出来进行对比，为了方便比较，一些目标函数的形式与论文中给出的形式不同，但是含义是相同的。

<table>
<thead>
<tr>
<th>模型</th><th>目标函数</th><th>类型</th><th>方法</th>
</tr>
</thead>
<tbody>
<tr>
<td>NNLM</td><td>$\max{\frac{1}{T}\sum_{t}^{}{\log{p\left( w_{t}|w_{t - n + 1}^{t - 1} \right)} + R(\theta)}}$</td><td rowspan="6">语言模型</td><td rowspan="6">神经网络</td>
</tr>
<tr>
<td>C&W</td><td>$\min{\sum_{s \in S}^{}{\sum_{w \in D}^{}{\max\left( 0,1 - f(s) + f\left( s^{w} \right) \right)}}}$</td>
</tr>
<tr>
<td>Global Context-Aware NLM</td><td>$\min{\sum_{w \in V}^{}{\max\left( 0,1 - g(s,d) + g\left( s^{w},d \right) \right)}}$</td>
</tr>
<tr>
<td>Skip-Gram (softmax)</td><td>$\max{\sum_{(w,c) \in D}^{}{\log{p\left( c|w \right)}}}$</td>
</tr>
<tr>
<td>Skip-Gram (SGNS)</td><td>$\max{\sum_{(w,c) \in D}^{}{\log{\sigma\left( v_{c} \cdot v_{w} \right)}} + \sum_{(w,c) \in D^{'}}^{}{\log{\sigma\left( - v_{c} \cdot v_{w} \right)}}}$</td>
</tr>
<tr>
<td>CBOW (softmax)</td><td>$\max{\sum_{w}^{}{\log{p\left( w|\left\{ c \in C(w) \right\} \right)}}}$</td>
</tr>
<tr style="border-top: 1px black solid;">
<td>SVD</td><td>分解$X = USV^{T}$</td><td rowspan="5">矩阵分解</td><td rowspan="2">代数迭代</td>
</tr>
<tr>
<td>NNSE</td><td>$\min{\sum_{i = 1}^{m}\left( \left| X_{i} - A_{i} \times D \right|^{2} + \lambda\left\| A_{i} \right\|_{1} \right)}$</td>
</tr>
<tr>
<td>GloVe</td><td>$\min{\sum_{i,j = 1}^{V}{f\left( X_{\text{ij}} \right)\left( w_{i}^{T}{\widetilde{w}}_{j} + b_{i} + {\widetilde{b}}_{j} - \log\left( X_{\text{ij}} \right) \right)^{2}}}$</td><td rowspan="3" style="border-top: 1px black solid;">普通非线性优化</td>
</tr>
<tr>
<td>Regression (Hashimoto et al., 2016)</td><td>$\exp\left( - \frac{1}{2}\left| x_{i} - x_{j} \right|^{2} + a_{i} + b_{j} \right) \rightarrow LL(x,a,b,\theta)$</td>
</tr>
<tr>
<td>Squared Norm (Arora et al., 2016)</td><td>$\min{\sum_{v,w}^{}{X_{\text{vw}}\left( \frac{|v + w|^{2}}{2d} - \log X_{\text{vw}} - C \right)^{2}}}$</td>
</tr>
</tbody>
</table>

其中概率函数p都是softmax，X是共现矩阵，regression (Hashimoto et al., 2016) 的函数LL是一个比较复杂的负二项分布的期望的表达式，可以参见原文。

从目标函数可以看出，基于语言模型的方法都会使用归一化的函数（softmax），导致复杂度比较高，解决的方法包括并行、层次化softmax和负采样。基于矩阵分解的模型都使用了共现矩阵，且目标函数的形式相似，如Arora et al. (2016) 的论文指出，GloVe的偏置量在该模型中有具体含义，即$b_{i} = w_{i}^{2}$，CBOW中的上下文词嵌入的均值$\frac{1}{K}\sum_{k = 1}^{K}w_{k}$是该模型中语义变量$c_{t}$的最大似然估计。

## 词嵌入表示的性能增强

### 低频词的处理

尽管各种词嵌入模型已经可以解决单词表示的问题，但要想实际使用这些模型还有一个很大的问题，就是它们对低频词的嵌入估计很差，或者说第3节中列举的常见词嵌入计算方法的泛化能力都很差。这里简单解释一下泛化能力差的原因。

首先考虑完全没有在训练语料库中出现的词（Out-of-Vocabulary, OOV）。无论是基于语言模型的模型还是基于共现矩阵分解的模型，都需要把单词一一映射到向量上，这种映射的具体实现方法可以是用矩阵下标或哈希表，但是无论怎样都只能把已知的单词作为key。所以这些模型对于完全没有出现过的词无能为力，需要引入更多假设或工程上的处理才行，例如在预处理阶段直接删除OOV。

再考虑语料库中出现频率很低的词。这种情况模型是能处理的，但是一般效果不好。一方面从信息量的角度上来讲，出现频率很低意味着我们无法得知关于这个词更多的含义。另一方面从模型训练的角度上来讲，上表列出的使用神经网络和普通非线性优化的方法都需要采用SGD、Adam等梯度下降的方法，在采样时选取低频数据的概率较小，导致这组数据的下降不充分。而使用代数迭代的方法会受到矩阵稀疏的影响，由于共现矩阵在低频词的行较为稀疏，特征相对不明显，所以在分解截断的过程中会倾向忽略仅有的信息。

这在具有long-tailed频率分布的形态丰富的语言中或词汇表变化很大的环境（例如社交媒体）中尤其有问题。要解决这个问题，可以从单词的构成入手，例如eventful、eventfully、uneventful和uneventfully具有相同的词根，实际上词义也是相关的。如果在训练过程中能有效利用subword information，那么在遇到低频词时也可以用词语的字符串信息进行推断。

#### Character-Aware Neural Language Models

Kim et al. (2015) 提出了一个字符感知的语言模型，该模型首先是字符embedding经过CNN，然后经过highway network，然后再作为LSTM的输入，最后经过一个softmax输出概率，这个概率的预测还是单词级别的。也就是说，该模型将词嵌入视为字符的卷积，训练是针对字符embedding的，也就不存在生词的问题了。

假设C为字符集合，d为character embeddings维度的大小，单词k由字符$\left\lbrack c_{1},\ldots,c_{l} \right\rbrack$组成，其中$l$为单词k的长度，那么单词k就可以表示为$C^{k} \in R^{d \cdot l}$。

首先$C^{k}$经过不同宽度的卷积核卷积，即$f^{k}\lbrack i\rbrack = \tanh\left( \left\langle C^{k}\lbrack*,i:i + w - 1\rbrack,H \right\rangle + b \right)$，然后对每种宽度做最大池化，即$y^{k} = \max_{i}{f^{k}\lbrack i\rbrack}$。将不同宽度的$y_{k}$拼接起来得到一个初步卷积后的单词表示。

这个单词表示可以经过一个可选的highway network，所谓highway network就是一个半透明的非线性层，transform gate和carry gate的大小也与输入有关，作为训练参数。

最后highway network的输出向量就是单词表示，它是用字符级别的表示通过不同宽度的卷积核卷积而成的，所以有不同长度的subword信息。

这篇论文的另一个贡献是在实验部分测试了在不同语言下的模型性能。评价指标是模型的困惑度（PPL）。测试结果显示，在英语方面，尽管该模型的参数减少了约60%，但是结果与Penn Treebank (PTB)现有的SOTA相当。在词法丰富的语言（阿拉伯语、捷克语、法语、德语、西班牙语和俄语）上，该模型优于各种基线（Kneser-Ney, word-level/morpheme-level LSTM），而且参数也更少。

但是这个模型的问题在于，后续每次需要新的词嵌入时，都要通过卷积运算才可以。

#### Subword N-gram

Bojanowski et al. (2017) 基于英文词缀的特点增强了skip-gram with negative sampling (SGNS) 模型。它与Character-Aware Neural Language Models相比忽略了subword的顺序信息，固定了subword的长度，因此也不需要卷积层。

例如一个单词“where”，如果选取超参数n=3，那么可以生成以下5个字符序列

$$G_{w = \text{where}} = \left\{ \text{<wh},\ \text{whe},\ \text{her},\ \text{ere},\ \text{re>} \right\}$$

如果是普通的SGNS，那么两个词嵌入间的距离计算方式就是内积$s(w,c) = u_{w}^{T}v_{c}$，其中u是目标词嵌入，v是上下文词嵌入。

而该作者提出了新的词嵌入距离计算方式

$$s(w,c) = \sum_{g \in G_{w}}^{}z_{g}^{T}v_{c}$$

其中z是词缀向量。可以理解为词嵌入就是所有长度为n的子串（前后各加一个padding）所对应的向量的和。

实验部分，主要的评价指标有三个：similarity、analogy和language model。这篇论文也测试了模型在不同语言下的性能，对于不同的语言，采用n-gram的字符级向量信息，带来的收益并非都成正比。对于类似德文这样多复合词的，会取得更好的效果，但是对英文来说，则不一定适用。另外，对于语法学（syntactic）的问题，该论文的模型表现得更优异，但是对于语意学（semantic）的问题，该论文的模型则不会比基线模型表现得更好，有时候甚至表现得更糟糕。最后的language model任务，作者采用的数据集语言包括捷克语、德语、西班牙语、法语和俄语，并与LSTM、skip-gram模型以及log-bilinear language model、character aware language model (Kim et al., 2015)进行了对比，结果发现该论文的引入subword information的效果最好。这显示了subword information在语言建模任务中的重要性，并展示了用于词法丰富的语言的向量的有效性。

关于上述两个模型的对比结果，我认为Subword N-gram好于Character-Aware Neural Language Models的原因在于它的基础结构，Subword N-gram用了skip-gram作为训练方法，而Character-Aware Neural Language Models用的是NNLM，这两者学习词嵌入的效果本身就有明显差异。我个人猜想，如果将两者的结构统一起来，也就是用skip-gram + character-aware的结构，或许也能实现较好的结果。

#### 中文字符加强Character-Enhanced Word Embedding模型

大多数词嵌入模型都是基于英文语料进行实验和测试的，包括前面两节提到的Character-Aware Neural Language Models和Subword N-gram两种加强低频词效果的方法，都是尝试提取单词中的内部结构来辅助推测词义，在英文中单词的词缀也确实有这种提示信息。但是在这方面中文可能比英文能做得更好，因为中文的词是由汉字构成的，而汉字相比于英文词缀有更具体的语义信息。

Chen et al. (2015) 提出了一种在词嵌入训练过程中加入字符信息的方法，称为Character-Enhanced Word Embedding (CWE) ，并以中文为主测试了该方法的效果。这个模型大致是，每个词和每个字都对应一个向量表示，最终一个词的表示是

$$x_{j} = w_{j} \oplus \frac{1}{N_{j}}\sum_{k = 1}^{N_{j}}c_{k}$$

其中$w_{j}$是词的表示，$c_{k}$是字的表示，$N_{j}$是该词的字符数，$x_{j}$是新的词嵌入。$\oplus$是词表示和字表示的连接方法，文中提到可以用addition或concatenation，其中addition要求词表示和字表示的长度相同，而concatenation没有这个要求，测试发现虽然concatenation比addition需要更多的训练时间，但是效果并没有更好，所以后续就只用了addition的方法进行测试，具体地说就是

$$x_{j} = \frac{1}{2}\left( w_{j} + \frac{1}{N_{j}}\sum_{k = 1}^{N_{j}}c_{k} \right)$$

除了这个模型以外，论文中还提出了两个细节上的处理。

第一是关于字义的区分。由于汉字存在比较大的歧义性，所以每个汉字只分配一个向量不足以表达它的含义。文中提出了三种多原型的字符表示方法：第一种是用位置来区分，因为字义很大程度上取决于它在词语中的位置，所以给每个字分配三种表示，$c^{B},\ c^{M},\ c^{E}$，分别表示该字在词的首位、中间和末位的情况。第二种是用聚类的方法，对于每个字$c$，将它所有出现的位置分为$N_{c}$类，对应$N_{c}$个向量，每次训练的过程中用上次训练的结果，选与上下文词嵌入最相似的那个作为这次训练的向量。第三种方法是非参数聚类，与第二种类似，只是每个字的类别数量是动态调整的，在训练过程中，如果遇到一个字的所有表示向量都与当前的上下文词嵌入差别很大，就再增加一个字向量。

第二是对非组成词的处理。非组成词是指词语的含义并不是由它的每个字的字义组成的，具体包括三种：单纯多字词（single-morpheme multi-character words），如“琵琶”“徘徊”，这些字一般不出现在别的词中；音译词（transliterated words），如“沙发”“巧克力”，这些词主要是由字音而不是字义构成的；实体名称（entity names），如人名、地名、组织名等。处理这些非组成词的方法是，对于单纯多字词无需特别处理，因为这些字只在这些词中出现，对模型没有负面影响；对于音译词和实体名称分别采用词表和词性标注的方法，在训练过程中只用他们的原始词嵌入表示，不再拆分使用他们的汉字嵌入表示。

论文中有模型的参数复杂度和计算复杂度的分析，然后是四个实验，分别是similarity、analogy、模型对语料库大小的灵敏度测试和两种多原型方法的定性对比。对比模型是CBOW、skip-gram、GloVe、单原型CWE和多种多原型CWE模型。在similarity和analogy两个任务上，多原型CWE模型的效果最好，因为它比普通的词嵌入模型多学习了字符级的信息，并且普通的词嵌入模型无法处理OOV，作者将这种情况下普通词嵌入模型的得分设为零。在两种多原型方法的对比中，发现基于位置的方法时好时坏，而基于聚类的方法在大多数情况下都很好，因为确实基于位置的方法比较粗糙，字义也不一定与它在词中的位置有很大关系。

### 多义词和同形异义词的处理

使用词嵌入时还有一个隐含的问题，就是自然语言中一个词可能有多种含义，而我们如果只用一种表示的话就不能很好地表示多义词语义特征。为了解决这个问题，目前有两种思路，一种是想办法将多义词的不同含义做不同的标记后再训练它们的表示，另一种是通过分解训练完成后的单个嵌入来获得多个表示。

Reisinger & Mooney (2010) 提出了一种使用聚类来为每个单词产生多个意义向量的方法，Huang et al. (2012) 将该方法应用到了他们的Global Context-Aware Neural Language Model上，并称为多原型神经语言模型。该方法概括为如下过程。

1. 针对一个词出现的所有位置设定一个固定大小的窗口（前后各5），对窗口中的词求加权平均权重。

2. 使用spherical k-means聚类方法对这些短句进行聚类。

3. 最后该词在其每个所属的类别中被重新标记，训练中视为多个词。

在实验中(Huang et al., 2012)，测试方法也分为定性和定量两种。定性方法是，在训练了多原型表示之后，对每个词的多个原型，如bank_1（银行）和bank_2（岸），分别选取3个词嵌入距离最近的词，看他们的词义是否相近。结果实例：bank_1: corporation, insurance, company；bank_2: shore, coast, direction，可以看到效果还是不错的。定量方法是，构造一个数据集，每组数据是两个词语和他们的上下文，由人类评估这两个词语在这些上下文语境下的相似性，最后的指标是相关系数。与其他数据集的不同之处在于，可能一组数据的两个词是完全一样的，但是他们在语境中的含义不同。这种情况下，普通的单原型词语表示方法一定会认为他们词义完全一致，但是该多原型表示方法可以捕获他们的不同语义。结果确实证明了多原型的表现要优于单原型。

Arora et al. (2018) 采用了另一种思路。这项工作证明了，在常见的词嵌入模型中，多义词的词嵌入实际上是它多个含义的线性组合。通过随机游走模型(Arora et al., 2016)的训练过程，可以提取出话语原语（discourse atom），这些话语原语就是多义词的多个语义，解释话语原语就看与这个原语关联最近的一些词。如，考虑一个多义词，比如tie，它可以指领带，或比赛平局，或打结。如果用tie1, tie2, tie3来表示每个单独的含义，那么GloVe和word2vec的计算结果满足

$$v_{\text{tie}} = \alpha_{1}v_{tie1} + \alpha_{2}v_{tie2} + \alpha_{3}v_{tie3} + \cdots$$

其中系数$\alpha_{i}$是该含义出现的频率占所有含义的比值。

推导过程是将随机游走模型(Arora et al., 2016)进行了修改，变为高斯游走模型，其中的话语向量c满足均值为0、协方差为$\Sigma$的高斯分布，每次运动时，产生一个窗口的单词。

实验部分首先测试了高斯游走模型和线性断言的有效性，然后用词义归纳的方法将该模型与其他多义词相关的处理方法进行了对比。词义归纳任务用了三种方法，分别是SemEval 2010、word similarity in context (Huang et al., 2012)和他们新提出的police lineup测试。其中SemEval 2010是聚类任务，要求把每个多义词的不同含义的上下文分在不同的类别中；word similarity in context是评分任务，在介绍Global Context-Aware Neural Language Model时已经描述过了；police lineup是判别任务，它的数据集是由WordNet构建的，每个单词的每个词义都用8个相关的单词表示，测试时将多个词的数据混合起来，让模型判断哪组数据是对应当前单词的。

在三个词义归纳任务中，前两个任务都对比了该模型与(Huang et al., 2012)的模型，结果是在SemEval 2010任务中该模型更优秀，在word similarity in context任务中Global Context-Aware Neural Language Model更优秀。这可能是因为该模型将词嵌入分解为了较少的话语原语，话语原语在聚类时可以隐式地作为中心，从而有更好的聚类效果，而GCANLM在训练过程中重点提取了全局信息，因此在上下文丰富的环境中效果更好。第三个任务对比了人类的测试结果，结果显示该模型与非英语母语研究生的测试结果相近，但是英语母语的人在这项任务上做得更好。

## 词嵌入模型的后处理

Mu & Viswanath (2018) 发现，通过对常用词嵌入模型的输出进行简单的后处理可以使得结果在多项评价指标上都优于不处理的结果。

这项工作的出发点是，他们发现训练出来的词嵌入有两个特点：

1.  词嵌入的均值非零；

2.  删除均值向量后，词嵌入并不满足之前 (Arora et al., 2016) 的各项同性的假设。

所以可以尝试将词嵌入进行一些平滑操作：

1.  所有的词嵌入减去均值

2.  将新的词嵌入排列在一起，计算PCA

3.  每个词嵌入再减去D个主成分（乘一个系数）

该论文的实验部分包含了四项评价，分别是Concept Categorization、Word Analogy、Semantic Textual Similarity和Supervised Classification。前两项属于内部评价，后两项属于外部评价。实验结果表示，这种简单的后处理方法对提升word2vec和GloVe模型的性能都有比较明显的效果。

作者认为，这种后处理方法之所以有效，是因为word2vec和GloVe都可以看成是基于PMI的模型，Arora et al. (2016) 在RAND-WALK模型中，用极大似然规则解释了这些PMI模型的原理，但是他的推导过程中假设词嵌入的均值为零且各向同性。理论上，我们期望神经网络能自动学习到合理的偏置和分布，但实际训练出的嵌入并不满足这两个假设。

在后处理时，各向量减去均值和主成分的过程就是人工将向量的均值归零，并削弱向量中最分散的维度，拉长向量中最集中在零值的维度。通过人工修正后的词嵌入具有更强的自归一化特性，更加各向同性，因此可以更符合预期的性质。

## 词嵌入的性质和评价

### 评价指标和相关数据集

评价词嵌入有两种方法，一种是intrinsic（内部评价），另一种是extrinsic（外部评价）

Intrinsic Evaluation（内部评价）是在一个中间任务上评价词嵌入的好坏。该方法计算速度快，能帮助我们更好地理解系统。

Extrinsic Evaluation（外部评价）是在一个真正的NLP任务（如文本分类、机器翻译）中使用词嵌入，以此来评判词嵌入的好坏。但是计算extrinsic任务会消耗很长的时间。即使extrinsic任务出现了问题，我们也不清楚是词嵌入的问题还是其他子系统的问题。

内部评价主要是针对于词嵌入常见的性质进行测试，检查其在多大程度上满足这些性质。具体主要包括：

- Analogy: king-man+woman=queen

  检查词嵌入的语义线性运算性质，可以采用3CosAdd或3CosMul的运算方法。

  可用的数据集：MSR’s analogy dataset (Mikolov et al., 2013c)、Google’s analogy dataset (Mikolov et al., 2013a)

- Word Similarity: 内积

  检查词嵌入的内积大小是否反映了词义的相关性，可以计算内积或欧氏距离与人类评价的相关系数。

  可用的数据集：WordSim353 (Finkelstein et al., 2001)、MEN dataset (Bruni et al., 2014)、SimLex-999 dataset (Hill et al., 2015)

另外还有如series completion、classification等指标。

外部评价是对具体任务实现效果的评价，常用的指标包括：命名实体识别、词性标注、情感分类等。

更多评价指标和数据集可以在ACL的网站上找到 https://aclweb.org/aclwiki/State_of_the_art 。

### 评价数据

很难给出一个结论，哪个词嵌入模型是最好的。例如，在目前已有的对比中，有些结论指出GloVe优于word2vec (Pennington et al., 2014)，而有些结论则相反 (Levy et al., 2015)。

下表是总结了几项工作中的实验部分similarity评价的对比结果。其中绿色背景色表示在这项工作中有该模型的评价数据，表格中为正数的项表示在该列中相关系数最大（最优）的模型，值是它的相关系数减去次优模型的相关系数，为0的项表示在该列中相关系数第二大（次优）的模型。

<table>
<thead>
<tr>
<th>方法</th><th>Levy et al.,2015</th><th>Pennington et al.,2014</th><th>Stratos et al.,2015</th>
</tr>
</thead>
<tbody style="text-align: center;">
<tr>
<td>PPMI</td><td style="background-color:#E2EFD9"></td><td></td><td></td>
</tr>
<tr>
<td>SVD</td><td style="background-color:#E2EFD9">0</td><td style="background-color:#E2EFD9">0</td><td style="background-color:#E2EFD9"></td>
</tr>
<tr>
<td>SGNS</td><td style="background-color:#E2EFD9">0.03</td><td style="background-color:#E2EFD9"></td><td style="background-color:#E2EFD9">0</td>
</tr>
<tr>
<td>CBOW</td><td style="background-color:#E2EFD9"></td><td style="background-color:#E2EFD9"></td><td style="background-color:#E2EFD9"></td>
</tr>
<tr>
<td>GloVe</td><td style="background-color:#E2EFD9"></td><td style="background-color:#E2EFD9">0.02</td><td style="background-color:#E2EFD9"></td>
</tr>
<tr>
<td>CCA</td><td></td><td></td><td style="background-color:#E2EFD9">0.01</td>
</tr>
</tbody>
</table>

下表是几项工作中的analogy评价。数据表示与上表是相同的，指标是准确率。

<table>
<thead>
<tr>
<th>方法</th><th>Levy et al.,2015</th><th>Pennington et al.,2014</th><th>Stratos et al.,2015</th>
</tr>
</thead>
<tbody style="text-align: center;">
<tr>
<td>PPMI</td><td style="background-color:#E2EFD9"></td><td></td><td></td>
</tr>
<tr>
<td>SVD</td><td style="background-color:#E2EFD9"></td><td style="background-color:#E2EFD9"></td><td style="background-color:#E2EFD9"></td>
</tr>
<tr>
<td>SGNS</td><td style="background-color:#E2EFD9">0.01</td><td style="background-color:#E2EFD9">0</td><td style="background-color:#E2EFD9">0.05</td>
</tr>
<tr>
<td>CBOW</td><td style="background-color:#E2EFD9"></td><td style="background-color:#E2EFD9"></td><td style="background-color:#E2EFD9"></td>
</tr>
<tr>
<td>GloVe</td><td style="background-color:#E2EFD9">0</td><td style="background-color:#E2EFD9">0.1</td><td style="background-color:#E2EFD9">0</td>
</tr>
<tr>
<td>CCA</td><td></td><td></td><td style="background-color:#E2EFD9"></td>
</tr>
</tbody>
</table>

具体实验条件：

-   Levy et al., 2015: 训练数据集大小1.5B，500维词嵌入，similarity测试数据集WordSim353、MEN、SimLex等，analogy测试数据集Google’s analogy dataset（多个测试数据集结果有差异，但综合来看是SGNS最好，SVD次之）。

-   Pennington et al., 2014: 训练数据集大小有6B和42B，100维、300维和1000维词嵌入，similarity测试数据集WordSim353等，analogy测试数据集MSR’s analogy dataset、Google’s analogy dataset（多个测试数据集结果相似）。

-   Stratos et al., 2015: 训练数据集大小1.4B，500维和1000维词嵌入，similarity测试数据集WordSim353、MEN、Stanford Rare Word的平均，analogy测试数据集MSR’s analogy dataset、Google’s analogy dataset（多个测试数据集结果相似）。

从指标的角度来看，各个模型在similarity任务上都有所长，但除了SGNS以外，主要还是基于矩阵分解的方法（SVD、GloVe、CCA）在similarity任务上表现较好。这是因为共现矩阵直接反映了词与词之间的相似性，在分解降维之前，统计数据里相似的词在相同的维度上就具有相关性，矩阵分解算法可以直观获得这一信息，特别是CCA模型在确定参数时使用了Brown聚类，词义相似的词分在同一块中，更加强了相似性信息；而基于语言模型的算法依赖多组数据输入，这样实际上不能直接得到词语间相似词义的信息，而要根据多个相似的上下文，经过多次相似的梯度下降迭代，间接实现相似词表示的训练。在analogy任务上，表现最好的是SGNS和GloVe，关于这一点，有一些工作尝试给出解释 (Arora et al., 2016)，基本思路是共现概率的比值表达了词义的线性关系，而GloVe本身就是基于共现概率的比值推导出的模型，SGNS已被证明是PMI（log共现概率）矩阵的近似分解，所以它的词嵌入的减法也可以看成共现概率的除法，这两者都与共现概率的比值有较大的相关性；其他的模型（如SVD、CCA）则没有针对共现概率比值的处理。

从实验的角度来看，这三个实验的评价结果有一些差异，我个人认为应该主要参考第一列的工作(Levy et al., 2015)，因为这篇论文的主要内容就是对比模型，对参数的理解和测试更深入。

为了便于今后开展相关工作，这里引用一个2019年的词嵌入模型的外部评价结果(Wang et al., 2019)。其中命名实体识别（NER）和情感分析（SA）的结果可能对专业语料的分类任务有帮助。

<table style="text-align:center;">
<thead>
  <tr>
    <th></th>
    <th rowspan="2">POS</th>
    <th rowspan="2">Chunking</th>
    <th rowspan="2">NER</th>
    <th colspan="2" style="border-bottom:1px black solid;">SA(IMDb)</th>
    <th colspan="2" style="border-bottom:1px black solid;">SA(SST)</th>
    <th>NMT</th>
  </tr>
  <tr>
    <th></th>
    <th>Bi-LSTM</th>
    <th>CNN</th>
    <th>Bi-LSTM</th>
    <th>CNN</th>
    <th>Perplexity</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>SGNS</td>
    <td style="font-weight:bold">94.54</td>
    <td>88.21</td>
    <td>87.12</td>
    <td>85.36</td>
    <td>88.78</td>
    <td>64.08</td>
    <td style="font-weight:bold">66.93</td>
    <td>79.14</td>
  </tr>
  <tr>
    <td>CBOW</td>
    <td>93.79</td>
    <td>84.91</td>
    <td>83.83</td>
    <td style="font-weight:bold">86.93</td>
    <td>85.88</td>
    <td>65.63</td>
    <td>65.06</td>
    <td>102.33</td>
  </tr>
  <tr>
    <td>GloVe</td>
    <td>93.32</td>
    <td>84.11</td>
    <td>85.3</td>
    <td>70.41</td>
    <td>87.56</td>
    <td>65.16</td>
    <td>65.15</td>
    <td>84.20</td>
  </tr>
  <tr>
    <td>FastText</td>
    <td>94.36</td>
    <td>87.96</td>
    <td>87.10</td>
    <td>73.97</td>
    <td>83.69</td>
    <td>50.01</td>
    <td>63.25</td>
    <td>82.60</td>
  </tr>
  <tr>
    <td>ngram2vec</td>
    <td>94.11</td>
    <td style="font-weight:bold">88.74</td>
    <td style="font-weight:bold">87.33</td>
    <td>79.32</td>
    <td style="font-weight:bold">89.29</td>
    <td style="font-weight:bold">66.27</td>
    <td>66.45</td>
    <td style="font-weight:bold">77.79</td>
  </tr>
  <tr>
    <td>Dict2vec</td>
    <td>93.61</td>
    <td>86.54</td>
    <td>86.82</td>
    <td>62.71</td>
    <td>88.94</td>
    <td>62.75</td>
    <td>66.09</td>
    <td>78.84</td>
  </tr>
</tbody>
</table>

总体上看，SGNS和ngram2vec（一种n-gram to n-gram的预测模型）在各项任务上表现较好（分类器使用CNN）。不过这个对比中基于共现矩阵分解的方法只有GloVe，没有体现出两类方法的性能差异。

下面再引用(Stratos et al., 2015)关于NER任务的对比，这个实验比较充分地展示了矩阵分解和神经网络模型在外部评价指标上的差异，实验中GloVe、CBOW和skip-gram都采用了推荐的默认配置，窗口大小是左右各2。

<table style="text-align:center;">
<thead>
  <tr>
    <th>Features</th>
    <th colspan="2">30 dimensions</th>
    <th colspan="2">50 dimensions</th>
  </tr>
  <tr>
    <th></th>
    <th>Dev</th>
    <th>Test</th>
    <th>Dev</th>
    <th>Test</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>—</td>
    <td>90.04</td>
    <td>84.40</td>
    <td>90.04</td>
    <td>84.40</td>
  </tr>
  <tr>
    <td>BROWN</td>
    <td>92.49</td>
    <td>88.75</td>
    <td>92.49</td>
    <td>88.75</td>
  </tr>
  <tr style="border-top:1px black solid;">
    <td>LOG</td>
    <td>92.27</td>
    <td>88.87</td>
    <td>92.91</td>
    <td style="font-weight:bold">89.67</td>
  </tr>
  <tr>
    <td>REG</td>
    <td>92.51</td>
    <td>88.08</td>
    <td>92.73</td>
    <td>88.88</td>
  </tr>
  <tr>
    <td>PPMI</td>
    <td>92.25</td>
    <td>89.27</td>
    <td>92.53</td>
    <td>89.37</td>
  </tr>
  <tr>
    <td>CCA</td>
    <td style="font-weight:bold">92.88</td>
    <td style="font-weight:bold">89.28</td>
    <td>92.94</td>
    <td>89.01</td>
  </tr>
  <tr style="border-top:1px black solid;">
    <td>GLOVE</td>
    <td>91.49</td>
    <td>87.16</td>
    <td>91.58</td>
    <td>86.80</td>
  </tr>
  <tr>
    <td>CBOW</td>
    <td>92.44</td>
    <td>88.34</td>
    <td>92.83</td>
    <td>89.21</td>
  </tr>
  <tr>
    <td>SKIP</td>
    <td>92.63</td>
    <td>88.78</td>
    <td style="font-weight:bold">93.11</td>
    <td>89.32</td>
  </tr>
</tbody>
</table>

这个结果实际上也体现出矩阵分解和神经网络两类方法的实际表现差别不大，其中CCA和skip-gram的F1得分略高一些，这可能得益于CCA和skip-gram可以较好地捕获词义（参考内部评价）。

Levy et al. (2015) 给出了他们对于词嵌入模型性能评价的态度，即词嵌入的性能提高很大程度上是由于特定系统设计选择和超参数优化，而不是词嵌入算法本身，并给出了一些超参数调整的建议：

-   始终使用上下文分布平滑（cds = 0.75）调整PMI，并且适用于PPMI、SVD和SGNS。

-   请勿“正确”使用SVD（eig = 1），而是使用对称变体之一。

-   SGNS是可靠的基准。尽管它可能不是每一项任务的最佳方法，但在任何情况下都不会明显地表现不佳。此外，SGNS是最快的训练方法，也是磁盘空间和内存消耗最佳的算法。

-   使用SGNS时，应该用较多的负样例。

-   对于SGNS和GloVe而言，值得尝试使用w+c的形式作为结果，这种尝试很容易（不需要重新训练），并且可以获得较多的收益（或较多的损失）。

## 参考文献

Sanjeev Arora, Yuanzhi Li, Yingyu Liang, Tengyu Ma, and Andrej Risteski. 2016. A latent variable model approach to pmi-based word embeddings. *Transactions of the Association for Computational Linguistics*, 4:385--399.

Sanjeev Arora, Yuanzhi Li, Yingyu Liang, Tengyu Ma, and Andrej Risteski. 2018. Linear algebraic structure of word senses, with applications to polysemy. *Transactions of the Association for Computational Linguistics*, 6:483--495.

Yoshua Bengio, Rejean Ducharme, Pascal Vincent, and Christian Jauvin. 2003. A neural probabilistic language model. *Journal of machine learning research*, 3(Feb):1137--1155.

Piotr Bojanowski, Edouard Grave, Armand Joulin, and Tomas Mikolov. 2017. Enriching word vectors with subword information. *Transactions of the Association for Computational Linguistics*, 5:135--146.

Elia Bruni, Nam-Khanh Tran, and Marco Baroni. 2014. Multimodal distributional semantics. *Journal of Artifificial Intelligence Research*, 49:1--47.

Xinxiong Chen, Lei Xu, Zhiyuan Liu, Maosong Sun, and Huanbo Luan. 2015. Joint learning of character and word embeddings. In *Twenty-Fourth International Joint Conference on Artifificial Intelligence*.

Ronan Collobert and Jason Weston. 2008. A unifified architecture for natural language processing: Deep neural networks with multitask learning. In *Proceedings of the 25th international conference on Machine learning*, pages 160--167.

Lev Finkelstein, Evgeniy Gabrilovich, Yossi Matias, Ehud Rivlin, Zach Solan, Gadi Wolfman, and Eytan Ruppin. 2001. Placing search in context: The concept revisited. In *Proceedings of the 10th international conference on World Wide Web*, pages 406--414.

John R Firth. 1957. A synopsis of linguistic theory, 1930-1955. *Studies in linguistic analysis*.

Yoav Goldberg and Omer Levy. 2014. word2vec explained: deriving mikolov et al.'s negative sampling word-embedding method. *arXiv preprint arXiv:1402.3722*.

Zellig S Harris. 1954. Distributional structure. *Word*, 10(2-3):146--162.

Tatsunori B Hashimoto, David Alvarez-Melis, and Tommi S Jaakkola. 2016. Word embeddings as metric recovery in semantic spaces. *Transactions of the Association for Computational Linguistics*, 4:273--286.

Felix Hill, Roi Reichart, and Anna Korhonen. 2015. Simlex-999: Evaluating semantic models with (genuine) similarity estimation. *Computational Linguistics*, 41(4):665--695.

Patrik O Hoyer. 2002. Non-negative sparse coding. In *Proceedings of the 12th IEEE Workshop on Neural Networks for Signal Processing*, pages 557--565. IEEE.

Eric H Huang, Richard Socher, Christopher D Manning, and Andrew Y Ng. 2012. Improving word representations via global context and multiple word prototypes. In *Proceedings of the 50th Annual Meeting of the Association for Computational Linguistics (Volume 1: Long Papers)*, pages 873--882.

David A Huffman. 1952. A method for the construction of minimum-redundancy codes. *Proceedings of the IRE*, 40(9):1098--1101.

Yoon Kim, Yacine Jernite, David Sontag, and Alexander M Rush. 2015. Character-aware neural language models. *arXiv preprint arXiv:1508.06615*.

Omer Levy and Yoav Goldberg. 2014. Neural word embedding as implicit matrix factorization. In *Advances in neural information processing systems*, pages 2177--2185.

Omer Levy, Yoav Goldberg, and Ido Dagan. 2015. Improving distributional similarity with lessons learned from word embeddings. *Transactions of the Association for Computational Linguistics*, 3:211--225.

Julien Mairal, Francis Bach, Jean Ponce, and Guillermo Sapiro. 2010. Online learning for matrix factorization and sparse coding. *Journal of Machine Learning Research*, 11(1).

Tomas Mikolov, Kai Chen, Greg Corrado, and Jeffrey Dean. 2013a. Effificient estimation of word representations in vector space. *arXiv preprint arXiv:1301.3781*.

Tomas Mikolov, Ilya Sutskever, Kai Chen, Greg S Corrado, and Jeff Dean. 2013b. Distributed representations of words and phrases and their compositionality. *Advances in neural information processing systems*, 26:3111--3119.

Tomas Mikolov, Wen-tau Yih, and Geoffrey Zweig. 2013c. Linguistic regularities in continuous space word representations. In *Proceedings of the 2013 conference of the north american chapter of the association for computational linguistics: Human language technologies*, pages 746--751.

Andriy Mnih and Geoffrey E Hinton. 2008. A scalable hierarchical distributed language model. *Advances in neural information processing systems*, 21:1081--1088.

Andriy Mnih and Koray Kavukcuoglu. 2013. Learning word embeddings effificiently with noise-contrastive estimation. In *Advances in neural information processing systems*, pages 2265--2273.

Frederic Morin and Yoshua Bengio. 2005. Hierarchical probabilistic neural network language model. In *Aistats*, volume 5, pages 246--252. Citeseer.

Jiaqi Mu and Pramod Viswanath. 2018. All-but-the-top: Simple and effective post-processing for word representations. In *6th International Conference on Learning Representations, ICLR 2018*.

Brian Murphy, Partha Talukdar, and Tom Mitchell. 2012. Learning effective and interpretable semantic models using non-negative sparse embedding. In *Proceedings of COLING 2012*, pages 1933--1950.

Jeffrey Pennington, Richard Socher, and Christopher D Manning. 2014. Glove: Global vectors for word representation. In *Proceedings of the 2014 conference on empirical methods in natural language processing (EMNLP)*, pages 1532--1543.

Joseph Reisinger and Raymond Mooney. 2010. Multiprototype vector-space models of word meaning. In *Human Language Technologies: The 2010 Annual Conference of the North American Chapter of the Association for Computational Linguistics*, pages 109--117.

Karl Stratos, Michael Collins, and Daniel Hsu. 2015. Model-based word embeddings from decompositions of count matrices. In *Proceedings of the 53rd Annual Meeting of the Association for Computational Linguistics and the 7th International Joint Conference on Natural Language Processing (Volume 1: Long Papers)*, pages 1282--1291.

Peter D Turney and Patrick Pantel. 2010. From frequency to meaning: Vector space models of semantics. *Journal of artifificial intelligence research*, 37:141--188.

Bin Wang, Angela Wang, Fenxiao Chen, Yuncheng Wang, and C-C Jay Kuo. 2019. Evaluating word embedding models: Methods and experimental results. *APSIPA transactions on signal and information processing*, 8.

Michael Zhai, Johnny Tan, and Jinho D Choi. 2016. Intrinsic and extrinsic evaluations of word embeddings. In *AAAI*, pages 4282--4283.
